# Copyright (c) 2014, Nick Palmius (University of Oxford)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the University of Oxford nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Contact: npalmius@googlemail.com
# Originally written by Nick Palmius, 23-Aug-2014

using SSM
using Plotly

include("..\\plotly-julia\\plotly_signin.jl");

function ssm_kalman_filter(data)
    θ = data.θ;

    x_forecast = [10.1:0.1:11];
    x = [data.x; x_forecast];

    μ = zeros(size(data.x));
    μ_predicted = zeros(size(x));
    μ_forecast = zeros(size(x_forecast));
    Σ = zeros(size(data.x));
    Σ_predicted = zeros(size(x));
    Σ_forecast = zeros(size(x_forecast));

    for i = 1:(length(data.x) + length(x_forecast))
        if i == 1
            μ_prev = 1.5;
            Σ_prev = 1;
        elseif i <= (length(data.x) + 1)
            μ_prev = μ[i - 1];
            Σ_prev = Σ[i - 1];
        else
            μ_prev = μ_forecast[i - length(data.x) - 1];
            Σ_prev = Σ_forecast[i - length(data.x) - 1];
        end

        μ_predicted[i] = θ.A(x[i]) * μ_prev + θ.B(x[i]) * θ.u(x[i]);
        Σ_predicted[i] = θ.A(x[i]) * Σ_prev * θ.A(x[i])' + θ.ε(x[i]).σ;

        y_hat = θ.C(x[i]) * μ_predicted[i] + θ.D(x[i]) * θ.u(x[i]);
        if i <= length(data.x)
            r = data.y[i] - y_hat;
        else
            r = 0;
        end

        S = θ.C(x[i]) * Σ_predicted[i] * θ.C(x[i])' + θ.δ(x[i]).σ;
        K = Σ_predicted[i] * θ.C(x[i])' * inv(S);

        μ_ = μ_predicted[i] + K * r;
        Σ_ = (1 - K * θ.C(x[i])) * Σ_predicted[i];

        if i <= length(data.x)
            μ[i] = μ_;
            Σ[i] = Σ_;
        else
            μ_forecast[i - length(data.x)] = μ_;
            Σ_forecast[i - length(data.x)] = Σ_;
        end
    end

    system_output = [
      "x" => data.x,
      "y" => data.z,
      "mode" => "lines+markers",
      "type" => "scatter",
      "name" => "System output"
    ];
    true_value = [
      "x" => data.x,
      "y" => data.y_true,
      "mode" => "lines",
      "type" => "scatter",
      "name" => "True value"
    ];
    observed = [
      "x" => data.x,
      "y" => data.y,
      "mode" => "markers",
      "type" => "scatter",
      "name" => "Observed data"
    ];
    kalman_filtered = [
      "x" => data.x,
      "y" => μ,
      "mode" => "lines",
      "type" => "scatter",
      "name" => "Kalman filtered data"
    ];
    predicted = [
      "x" => x_forecast,
      "y" => μ_forecast,
      "mode" => "lines",
      "type" => "scatter",
      "name" => "Forecast data"
    ];
    confidence = [
      "x" => x,
      "y" => [(μ .+ Σ); (μ_forecast .+ Σ_forecast)],
      "mode" => "lines",
      "type" => "scatter",
      "name" => "Variance"
    ];

    plot_data = [system_output, true_value, observed, kalman_filtered, predicted, confidence]

    response = Plotly.plot([plot_data], ["filename" => "ssm-kalman-filter", "fileopt" => "overwrite"]);
    plot_url = response["url"];

    print(plot_url);

    return μ, Σ;
end
