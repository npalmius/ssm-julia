# Copyright (c) 2014, Nick Palmius (University of Oxford)
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the University of Oxford nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

# Contact: npalmius@googlemail.com
# Originally written by Nick Palmius, 23-Aug-2014

using SSM
using Plotly

include("..\\plotly-julia\\plotly_signin.jl");

function ssm_generative(θ)
    data = SSM_Data();

    x = [1:0.1:10];

    z = zeros(size(x));
    y = zeros(size(x));
    y_true = zeros(size(x));

    for i = 1:length(y)
        if i == 1
            z[i] = θ.B(x[i]) * θ.u(x[i]);
        else
            z[i] = θ.A(x[i]) * z[i - 1] + θ.B(x[i]) * θ.u(x[i]) + rand(θ.ε(x[i]))
        end
        y[i] = θ.C(x[i]) * z[i] + θ.D(x[i]) * θ.u(x[i]) + rand(θ.δ(x[i]));
        y_true[i] = θ.B(x[i]) * θ.u(x[i]);
    end

    data.θ = θ;
    data.x = x;
    data.y = y;
    data.z = z;
    data.y_true = y_true;

    system_output = [
      "x" => x,
      "y" => z,
      "mode" => "lines+markers",
      "type" => "scatter",
      "name" => "System output"
    ];
    true_value = [
      "x" => x,
      "y" => y_true,
      "mode" => "lines",
      "type" => "scatter",
      "name" => "True value"
    ];
    observed = [
      "x" => x,
      "y" => y,
      "mode" => "markers",
      "type" => "scatter",
      "name" => "Observed data"
    ];

    plot_data = [system_output, true_value, observed];

    response = Plotly.plot([plot_data], ["filename" => "ssm-generative", "fileopt" => "overwrite"]);
    plot_url = response["url"];

    print(plot_url);

    return data;
end
